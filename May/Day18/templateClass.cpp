#include <cmath>
#include <cstddef>
#include <iostream>
template <class T> class Number {
private:
  T arr[5];

public:
  void inputArr(int n) {
    for (int i = 0; i < n; i++) {
      std::cout << "input arr[" << i << "]: ";
      std::cin >> this->arr[i];
    }
  }
  void outputArr(int n) {
    std::cout << "-----------------\n";
    for (int i = 0; i < n; i++) {
      std::cout << "output arr[" << i << "]: ";
      std::cout << this->arr[i] << std::endl;
    }
  }
  bool checkSonguyenTo(int n) {
    if (n < 2)
      return 0;
    for (int i = 2; i <= std::sqrt(n); i++) {
      if (n % i == 0)
        return 0;
    }
    return 1;
  }
  void SoNguyenTo(int n) {
    for (int i = 0; i < n; i++) {
      if (checkSonguyenTo(this->arr[i]) == true)
        std::cout << this->arr[i] << " ";
    }
  }
  bool checkSochinhPhuong(int n) {
    if (n <= 1)
      return false;
    if (std::sqrt(n) == (int)std::sqrt(n))
      return true;
    return false;
  }
  void SoChinhPhuong(int n) {
    for (int i = 0; i < n; i++) {
      if (checkSochinhPhuong(this->arr[i]) == true)
        std::cout << this->arr[i] << " ";
    }
  }
  bool checkSohoanhao(int n) {
    int sum = 0;
    for (int i = 1; i < n; i++) {
      if (n % i == 0)
        sum += i;
    }
    if (sum == n)
      return true;
    return false;
  }
  void soHoanhao(int n) {
    for (int i = 0; i < n; i++) {
      if (checkSohoanhao(this->arr[i]) == true)
        std::cout << this->arr[i] << " ";
    }
  }
  void Maxmin(int n) {
    T max = this->arr[0];
    T min = this->arr[0];
    for (int i = 0; i < n; i++) {
      if (this->arr[i] > max == true) {
        max = this->arr[i];
      }
      if (this->arr[i] < min == true) {
        min = this->arr[i];
      }
    }
    std::cout << "Max: " << max;
    std::cout << "\nMin: " << min << std::endl;
  }
};
int main(int argc, char *argv[]) {
  std::cout << "kieu du lieu int\n";
  Number<int> num1;
  num1.inputArr(5);
  num1.outputArr(5);
  std::cout << "so nguyen to: ";
  num1.SoNguyenTo(5);
  std::cout << std::endl;
  std::cout << "so chinh phuong: ";
  num1.SoChinhPhuong(5);
  std::cout << std::endl;
  std::cout << "so hoan hao: ";
  num1.soHoanhao(5);
  std::cout << std::endl;
  num1.Maxmin(5);

  Number<float> num2;
  std::cout << "\nkieu du lieu float\n";
  num2.inputArr(5);
  num2.outputArr(5);
  std::cout << "so chinh phuong: ";
  num2.SoChinhPhuong(5);
  std::cout << std::endl;
  num2.Maxmin(5);
  return 0;
}
