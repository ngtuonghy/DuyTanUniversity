#ifndef STUDENT_H
#define STUDENT_H

#include "person.h"
#include <cstdio>
class Student : public Person {
public:
  int studID;
  int avgMark;
  void input();
  void output();
  double calculateBonuns();
};
#endif // DEBUG
