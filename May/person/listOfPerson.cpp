#include "listOfPerson.h"
#include "lecturer.h"
#include "student.h"
#include <cstdio>
#include <type_traits>

void listOfPerson::inputEmployee() {
  int chon, i;
  k = 0;
  while (true) {
    cout << "\nStudent/Lecturer/stop(1/2/3):";
    do {
      cin >> chon;
      cin.get();
    } while (chon < 1 || chon > 3);
    if (chon == 3)
      break;
    if (chon == 1)
      ds[k] = new Student();
    if (chon == 2)
      ds[k] = new Lecturer();
    ds[k]->input();
    k++;
  }
}
void listOfPerson::outputEmployee() {
  for (int i = 0; i < k; i++) {
    ds[i]->output();
    printf("sau khi tinh tien: %2f \n", ds[i]->calculateBonuns());
  }
}
void listOfPerson::sortDESC() {
  for (int i = 0; i < k - 1; i++) {
    for (int j = i + 1; j < k; j++) {
      if (ds[i]->calculateBonuns() < ds[j]->calculateBonuns()) {
        swap(ds[i], ds[j]);
      }
    }
  }
}
