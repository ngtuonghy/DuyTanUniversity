#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>
using namespace std;
class Person {
public:
  int id;
  string name;
  virtual void input();
  virtual void output();
  virtual double calculateBonuns();
};
#endif // !EMPLOYEE_H
