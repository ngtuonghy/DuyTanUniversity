#include <iostream>
#include <vector>

int main(int argc, char *argv[]) {
  int arr[7] = {1, 2, 3, 4, 5, 6, 7};
  std::cout << "truoc khi quay: ";
  for (auto m : arr) {
    std::cout << m << " ";
  }
  int n;
  std::cin >> n;
  int count = 0;
  for (int i = n; i < 7 || count == n; i++) {
    std::swap(arr[i], arr[count]);
    count++;
  }
  std::cout << std::endl;
  for (auto x : arr)
    std::cout << x << " ";
  return 0;
}
