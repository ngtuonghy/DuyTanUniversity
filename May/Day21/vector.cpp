#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;
void output(vector<int> &v) {
  for (int x = 0; x < 8; x++) {
    cout << v[x] << " ";
  }
}
void Sort(vector<int> &v) { sort(v.begin(), v.end()); }
void Revese(vector<int> &v) { reverse(v.begin(), v.end()); }
void Rotate(vector<int> &v) { rotate(v.begin(), v.begin() + 3, v.end()); }
int main(int argc, char *argv[]) {
  vector<int> v;
  v.push_back(1);
  v.push_back(2);
  v.push_back(3);
  v.push_back(4);
  v.push_back(5);
  v.push_back(6);
  v.push_back(7);
  v.push_back(8);
  output(v);
  cout << endl;
  Sort(v);
  output(v);
  cout << endl;
  // Revese(v);
  output(v);
  cout << endl;
  Rotate(v);
  output(v);
  return 0;
}
