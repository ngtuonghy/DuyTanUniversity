#ifndef REGULAREMP_H
#define REGULAREMP_H

#include "employee.h"

class RegularEmp : public employee {
public:
  int empID;
  int salaryGrading;
  void input();
  void output();
  double calculateBonuns();
};
#endif // DEBUG
