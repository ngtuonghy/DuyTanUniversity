#include "listOfPerson.h"
#include <cstdio>
#include <type_traits>

void listOfPerson::inputEmployee() {
  int chon, i;
  k = 0;
  while (1) {
    cout << "\nRegularEmp/ContractEmp/stop(1/2/3):";
    cin >> chon;
    cin.get();
    if (chon == 3)
      break;
    if (chon == 1)
      ds[k] = new RegularEmp();
    if (chon == 2)
      ds[k] = new ContractEmp();
    ds[k]->input();
    k++;
  }
}
void listOfPerson::outputEmployee() {
  for (int i = 0; i < k; i++) {
    ds[i]->output();
    printf("sau khi tinh tien: %2f \n", ds[i]->calculateBonuns());
  }
}
void listOfPerson::sortDESC() {
  for (int i = 0; i < k - 1; i++) {
    for (int j = i + 1; j < k; j++) {
      if (ds[i]->calculateBonuns() < ds[j]->calculateBonuns()) {
        swap(ds[i], ds[j]);
      }
    }
  }
}
