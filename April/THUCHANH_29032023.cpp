#include <algorithm>
#include <iostream>
#include <istream>
#include <iterator>
#include <ostream>
#include <string>
#include <type_traits>

using std::cin;
using std::cout;
using std::endl;
using std::string;

class Vehicle {
public:
  string numberSign;
  string color;
  string source;

  Vehicle(string n = "null", string c = "null", string s = "null") {
    numberSign = n;
    color = c;
    source = s;
  }
  float cash() { return 0; }
  void input() {
    cout << "numberSign: ";
    cin >> numberSign;
    cout << "color: ";
    cin >> color;
    cout << "source: ";
    cin >> source;
  }
  void output() {
    cout << "numberSign: ";
    cout << numberSign << endl;
    cout << "color: ";
    cout << color << endl;
    cout << "source: ";
    cout << source << endl;
  }
};
class Car : public Vehicle {
public:
  string manufacturer;
  float priceWithVAT;
  float priceWithoutVAT;
  Car(string m = "", float pw = 0, float pwt = 0) {
    manufacturer = m;
    priceWithVAT = pw;
    priceWithoutVAT = pwt;
  }
  void input() {
    cout << "\n---------------------\n";
    Vehicle::input();
    cout << "enter manufacturer: ";
    cin >> manufacturer;
    cout << "enter priceWithVAT: ";
    cin >> priceWithVAT;
    cout << "enter priceWithoutVAT: ";
    cin >> priceWithoutVAT;
  }
  float cash() {
    if (priceWithoutVAT <= 2000) {
      priceWithVAT = priceWithoutVAT + priceWithoutVAT * 10 / 100;
    } else {
      priceWithVAT = priceWithoutVAT + priceWithoutVAT * 10 / 100;
    }
    return priceWithVAT;
  }
};

std::istream &operator>>(std::istream &CIN, Car &car) {
  cout << "numberSign: ";
  CIN >> car.numberSign;
  cout << "color: ";
  CIN >> car.color;
  cout << "source: ";
  CIN >> car.source;
  cout << "enter manufacturer: ";
  CIN >> car.manufacturer;
  cout << "enter priceWithVAT: ";
  CIN >> car.priceWithVAT;
  cout << "enter priceWithoutVAT: ";
  CIN >> car.priceWithoutVAT;

  return CIN;
}
std::ostream &operator<<(std::ostream &COUT, Car &car) {
  cout << "numberSign: ";
  COUT << car.numberSign << endl;
  cout << "color: ";
  COUT << car.color << endl;
  cout << "source: ";
  COUT << car.source << endl;
  cout << "enter manufacturer: ";
  COUT << car.manufacturer;
  cout << "enter priceWithVAT: ";
  COUT << car.priceWithVAT;
  cout << "enter priceWithoutVAT: ";
  COUT << car.priceWithoutVAT;

  return COUT;
}
class CarList {
  int n = 3;

public:
  /* void setn(int number) { n = number; } */
  /* int getn() { return n; } */
  /**/
  Car *car = new Car[n];
  void intputCar() {
    for (int i = 0; i < n; i++) {
      cin >> car[i];
    }
  }
  void outputCar() {
    cout << "\n----------------------\n";
    for (int i = 0; i < n; i++) {
      cout << car[i];
      cout << "\n------------------------\n";
    }
  }
  void sort() {
    int min;
    for (int i = 0; i < n; i++) {
      min = i;
      for (int j = i + 1; j < n; j++) {
        if (car[min].priceWithoutVAT > car[j].priceWithoutVAT) {
          min = j;
        }
      }
      if (min != i) {
        std::swap(car[i], car[min]);
      }
    }
  }
  int sumOfCash() {
    int sum{};
    for (int i = 0; i < n; i++) {
      sum += car[i].priceWithVAT;
    }
    return sum;
  }
  void maxCash() {
    int max = car[0].priceWithoutVAT;
    int count_max = 0;
    for (int i = 1; i < n; i++) {
      if (car[i].priceWithoutVAT > max)
        max = car[i].priceWithoutVAT;
      count_max = i;
    }
    cout << "\n" << car[count_max];
  }
};

int main(int argc, char *argv[]) {
  Vehicle *car1 = new Vehicle;
  Car *car2 = new Car;
  /* car1->input(); */
  /* car1->output(); */
  /* car2->input(); */
  /* car2->output(); */
  /* cout << "priceWithVAT: " << car2->cash(); */
  CarList carlist;
  /* cout << "nhap so phan tu mang: "; */
  /* int x; */
  /* cin >> x; */
  /* carlist.setn(5); */
  cout << "\nA.#input#\n";
  carlist.intputCar();
  cout << "\nB.#output#\n";
  carlist.outputCar();
  carlist.sort();
  cout << "\nC.#sau khi sap xep#";
  carlist.outputCar();
  cout << "sum of cash: " << endl << carlist.sumOfCash();
  cout << "\nD.#maxCash: ";
  carlist.maxCash();
  return 0;
}
