#include <iostream>
#include <istream>
#include <iterator>
#include <new>
#include <string>
int MAX = 2;
using std::cin;
using std::cout;
using std::endl;
using std::string;
// using namespace std;
class Xe {
public:
  string so_hieu;
  string mau_sac;
  string nguon_goc;
};
class Xemay : public Xe {
public:
  string hangsx;
  int giacoVAT;
  int giachuaVAT;
  void nhap() {
    cout << "Sohieu: ";
    cin >> so_hieu;
    cout << "Mau: ";
    cin >> mau_sac;
    cout << "nguon_goc: ";
    cin >> nguon_goc;
    cout << "khong_thue_VAT: ";
    cin >> giachuaVAT;
    cout << "co_thue_VAT: ";
    cin >> giacoVAT;
    cout << endl;
  }
  int tinhgiaban() {
    if (giachuaVAT <= 2000) {
      giacoVAT = giachuaVAT + giachuaVAT * 10 / 100;
    } else {
      giacoVAT = giachuaVAT + giachuaVAT * 15 / 100;
    }
    return giacoVAT;
  }
};
std::istream &operator>>(std::istream &CIN, Xemay &xm) {
  cout << "Sohieu: ";
  CIN >> xm.so_hieu;
  cout << "Mau: ";
  CIN >> xm.mau_sac;
  cout << "nguon_goc: ";
  CIN >> xm.nguon_goc;
  cout << "khong_thue_VAT: ";
  CIN >> xm.giachuaVAT;
  cout << "co_thue_VAT: ";
  CIN >> xm.giacoVAT;
  cout << endl;
  return CIN;
}
std::ostream &operator<<(std::ostream &COUT, Xemay &xm) {
  cout << "Sohieu: ";
  COUT << xm.so_hieu << endl;
  cout << "Mau: ";
  COUT << xm.mau_sac << endl;
  cout << "nguon_goc: ";
  COUT << xm.nguon_goc;
  cout << "khong_thue_VAT: ";
  COUT << xm.giachuaVAT << endl;
  cout << "co_thue_VAT: ";
  COUT << xm.giacoVAT << endl;
  cout << "-----------------\n";
  return COUT;
}
class DMxe {
public:
  Xemay *xe = new Xemay[MAX];
  void nhapxe() {
    for (int i = 0; i < MAX; i++) {
      cout << "xe thu: " << i << endl;
      cin >> xe[i];
    }
  }
  int tinhtongtien() {
    int tong{};
    for (int i = 0; i < MAX; i++) {
      tong += xe[i].tinhgiaban();
    }
    return tong;
  }
  void hienthi() {
    for (int i = 0; i < MAX; i++) {
      cout << "xe thu: " << i << endl;
      cout << xe[i];
    }
  }
  void timmaxmin() {
    int max = xe[0].giachuaVAT;
    int cout_max = 0;
    int min = xe[0].giachuaVAT;
    int cout_min = 0;
    for (int i = 1; i < MAX; i++) {
      if (xe[i].giachuaVAT > max) {
        max = xe[i].giachuaVAT;
        cout_max = i;
      }
      if (xe[i].giachuaVAT < min) {
        min = xe[i].giachuaVAT;
        cout_min = i;
      }
    }
    cout << "Max:\n";
    cout << xe[cout_max];
    cout << "Min:\n";
    cout << xe[cout_min];
  }
  int timxe() {
    string name;
    cout << "nhap so hieu can tim: ";
    cin >> name;
    for (int i = 0; i < MAX; i++) {
      if (name == xe[i].so_hieu)
        return 1;
    }
    return 0;
  }
};
int main(int argc, char *argv[]) {
  Xemay *xe1 = new Xemay;
  xe1->nhap();
  cout << "gia ban la: " << xe1->tinhgiaban();
  cout << "\n---------------------";
  cout << endl;
  DMxe listArray;
  cout << "------nhap-------\n";
  listArray.nhapxe();
  cout << "------xuat-------\n";
  listArray.hienthi();
  cout << endl;
  cout << "tong tien da co VAT la: " << listArray.tinhtongtien() << endl;
  cout << "\n---------maxmin----------\n";
  listArray.timmaxmin();
  cout << "\ntim-xe\n";
  if (listArray.timxe() == true) {
    cout << "da tim thay";
  } else {
    cout << "khong co ket qua";
  }
  return 0;
}
