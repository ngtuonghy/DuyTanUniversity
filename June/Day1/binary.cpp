#include <algorithm>
#include <iostream>
#include <iterator>
#include <stack>
#include <vector>
void stack(int n) {
  std::stack<int> stack;
  while (n > 0) {
    int x;
    x = n % 2;
    stack.push(x);
    n /= 2;
  }
  while (!stack.empty()) {
    std::cout << stack.top();
    stack.pop();
  }
}
int main(int argc, char *argv[]) {
  int n;
  std::cin >> n;
  std::vector<int> nhiphan;
  int a = n;
  while (a > 0) {

    int x;
    x = a % 2;
    nhiphan.push_back(x);
    a /= 2;
  }
  std::reverse(nhiphan.begin(), nhiphan.end());
  std::cout << "use vector: ";
  for (auto x : nhiphan) {
    std::cout << x;
  }
  std::cout << std::endl;
  std::cout << "use stack: ";
  stack(n);
  return 0;
}
