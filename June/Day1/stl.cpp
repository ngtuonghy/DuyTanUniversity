#include <algorithm>
#include <functional>
#include <iostream>
#include <list>
#include <queue>
#include <stack>
#include <vector>

void inputVector(std::vector<int> &arr, int n) {
  for (int i = 0; i < n; i++) {
    int x;
    std::cin >> x;
    arr.push_back(x);
  }
}
void outputVector(std::vector<int> arr) {
  std::vector<int>::iterator it;
  for (it = arr.begin(); it != arr.end(); it++) {
    std::cout << *it << " ";
  }
}
void sortarrAZ(std::vector<int> &arr, int n) {

  std::sort(arr.begin(), arr.end());
}
void sortarrZA(std::vector<int> &arr, int n) {

  std::sort(arr.begin(), arr.end(), std::greater<int>());
}
/*list*/
void inputList(std::list<int> &danhsach, int n) {
  for (int i = 0; i < n; i++) {
    int x;
    std::cin >> x;
    danhsach.push_front(x);
  }
}
void outputList(std::list<int> &danhsach, int n) {
  std::list<int>::iterator it;
  for (it = danhsach.begin(); it != danhsach.end(); it++) {
    std::cout << *it << " ";
  }
}
void sortListAz(std::list<int> &danhsach, int n) { danhsach.sort(); }
void sortListZa(std::list<int> &danhsach, int n) {
  danhsach.sort(std::greater<int>());
}
/*queue*/
void inputQueue(std::queue<int> &hangdoi, int n) {
  for (int i = 0; i < n; i++) {
    int x;
    std::cin >> x;
    hangdoi.push(x);
  }
}
void outputQueue(std::queue<int> hangdoi, int n) {
  while (!hangdoi.empty()) {
    std::cout << hangdoi.front() << " ";
    hangdoi.pop();
  }
}
void sortQueueAz(std::queue<int> &hangdoi) {
  std::vector<int> arr;
  while (!hangdoi.empty()) {
    arr.push_back(hangdoi.front());
    hangdoi.pop();
  }
  std::sort(arr.begin(), arr.end());
  for (int i = 0; i < arr.size(); i++) {
    hangdoi.push(arr[i]);
  }
}
void sortQueueZa(std::queue<int> &hangdoi) {
  std::vector<int> arr;
  while (!hangdoi.empty()) {
    arr.push_back(hangdoi.front());
    hangdoi.pop();
  }
  std::sort(arr.begin(), arr.end(), std::greater<int>());
  for (int i = 0; i < arr.size(); i++) {
    hangdoi.push(arr[i]);
  }
}
/*stack*/
void inputStack(std::stack<int> &nganxep, int n) {
  for (int i = 0; i < n; i++) {
    int x;
    std::cin >> x;
    nganxep.push(x);
  }
}
void outputStack(std::stack<int> nganxep, int n) {
  while (!nganxep.empty()) {
    std::cout << nganxep.top() << " ";
    nganxep.pop();
  }
}
void sortStackAz(std::stack<int> &nganxep) {
  std::vector<int> arr;
  while (!nganxep.empty()) {
    arr.push_back(nganxep.top());
    nganxep.pop();
  }
  std::sort(arr.begin(), arr.end());
  for (int i = 0; i < arr.size(); i++) {
    nganxep.push(arr[i]);
  }
}
void sortStackZa(std::stack<int> &nganxep) {
  std::vector<int> arr;
  while (!nganxep.empty()) {
    arr.push_back(nganxep.top());
    nganxep.pop();
  }
  std::sort(arr.begin(), arr.end(), std::greater<int>());
  for (int i = 0; i < arr.size(); i++) {
    nganxep.push(arr[i]);
  }
}

int main(int argc, char *argv[]) {
  /*vector*/
  std::cout << "Vector STL \n";
  std::vector<int> arr;
  inputVector(arr, 5);
  std::cout << "\nmang vua nhap: ";
  outputVector(arr);
  std::cout << "\nsap xep tang dan: ";
  sortarrAZ(arr, 5);
  outputVector(arr);
  std::cout << "\nsap xep giam dan: ";
  sortarrZA(arr, 5);
  outputVector(arr);
  /*list*/
  std::cout << "\nList STL \n";
  std::list<int> list;
  inputList(list, 5);
  std::cout << "\ndanh sach lien ket doi vua nhap: ";
  outputList(list, 5);
  std::cout << std::endl;
  std::cout << "sap xep tang dan dan: ";
  sortListAz(list, 5);
  outputList(list, 5);
  std::cout << std::endl;
  std::cout << "sap xep giam dan: ";
  sortListZa(list, 5);
  outputList(list, 5);

  /* *queue*/
  std::cout << "\nQueue STL \n";
  std::queue<int> queue;
  inputQueue(queue, 5);
  std::cout << "\ndanh sach hang doi vua nhap: ";
  outputQueue(queue, 5);
  sortQueueAz(queue);
  std::cout << "\ndanh sach hang doi sau sap xep: ";
  outputQueue(queue, 5);
  sortQueueZa(queue);
  std::cout << "\ndanh sach hang doi sau sap xep: ";
  outputQueue(queue, 5);

  /*stack*/
  std::cout << "\nStack STL \n";
  std::stack<int> stack;
  inputStack(stack, 5);
  std::cout << "danh sach ngan xep vua nhap: ";
  outputStack(stack, 5);
  std::cout << std::endl;
  std::cout << "sap xep giam dan: ";
  sortStackAz(stack);
  outputStack(stack, 5);
  std::cout << std::endl;
  std::cout << "sap xep tang dan: ";
  sortStackZa(stack);
  outputStack(stack, 5);
  return 0;
}
